﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Collections;
using Unity.Transforms;

namespace com.iam2bam {

	public class TestManager : MonoBehaviour {
		public int amount = 5000;
		public GameObject prefab;
		public Bounds bounds;
		protected EntityManager _emgr;
		public bool colorGradient = false;

		protected Vector3 RandomPos() {
			return new Vector3(Random.Range(bounds.min.x, bounds.max.x), Random.Range(bounds.min.y, bounds.max.y), Random.Range(bounds.min.z, bounds.max.z));
		}

		protected void Start() {
			_emgr = World.Active.GetOrCreateManager<EntityManager>();

			NativeArray<Entity> newEnts = new NativeArray<Entity>(amount, Allocator.Temp);
			_emgr.Instantiate(prefab, newEnts);
			for(int i = 0; i < newEnts.Length; i++) {
				var ent = newEnts[i];
				Unity.Mathematics.float3 pos;
				Color color;
				if(colorGradient) {
					var t = i / (float)newEnts.Length;
					var pc = Mathf.Lerp(bounds.min.x, bounds.max.x, t);
					pos.x = pos.y = pos.z = pc;
					color = Color.HSVToRGB(t, 1, 1);
				}
				else {
					pos = RandomPos();
					color = Random.ColorHSV(0, 1, 1, 1, 1, 1);
				}

				_emgr.SetComponentData(ent, new Position { Value = pos });
				
				if(_emgr.HasComponent<Dat_CustomRenderData>(ent))
					_emgr.SetComponentData(ent, new Dat_CustomRenderData { color = color });
			}

			//Make half static
			for(int i = 0; i < newEnts.Length / 2; i++) {
				_emgr.AddComponentData(newEnts[i*2], new Static { });
			}

			newEnts.Dispose();
		}
	}
}