﻿using Unity.Entities;
using UnityEngine;
using UnityEngine.Experimental.Rendering;
using Unity.Rendering;

namespace com.iam2bam
{
    [ExecuteInEditMode]
    public class Sys_MeshInstanceRendererBootstrap : ComponentSystem
    {
        protected override void OnCreateManager()
        {
            RenderPipeline.beginCameraRendering += OnBeforeCull;
            Camera.onPreCull += OnBeforeCull;

			//Disable old system
			m_MeshRendererSystem.Enabled = false;
        }

        protected override void OnUpdate()
        {
        }

#pragma warning disable 649
        [Inject]
        Sys_MeshInstanceRendererWithMPB m_RendererWithMPB;

        [Inject]
        Sys_MeshInstanceRendererWithoutMPB m_RendererWithoutMPB;

        [Inject]
        MeshInstanceRendererSystem m_MeshRendererSystem;

        [Inject] 
        LODGroupSystem m_LODSystem;
#pragma warning restore 649      
        public void OnBeforeCull(Camera camera)
        {
#if UNITY_EDITOR && UNITY_2018_3_OR_NEWER
            var prefabEditMode = UnityEditor.SceneManagement.StageUtility.GetCurrentStageHandle() !=
                                 UnityEditor.SceneManagement.StageUtility.GetMainStageHandle();
            var gameCamera = (camera.hideFlags & HideFlags.DontSave) == 0;
            if (prefabEditMode && !gameCamera)
                return;
#endif
            
            m_LODSystem.ActiveCamera = camera;
            m_LODSystem.Update();
            m_LODSystem.ActiveCamera = null;

			m_RendererWithMPB.ActiveCamera = camera;
            m_RendererWithMPB.Update();
            m_RendererWithMPB.ActiveCamera = null;

			m_RendererWithoutMPB.ActiveCamera = camera;
            m_RendererWithoutMPB.Update();
            m_RendererWithoutMPB.ActiveCamera = null;            
        }
    }
}
