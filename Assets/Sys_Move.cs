﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Unity.Collections;
using Unity.Mathematics;
using Unity.Entities;
using Unity.Jobs;
using Unity.Transforms;
using UnityEngine;

namespace com.iam2bam {
	class Sys_Move : JobComponentSystem {
		struct MoveJob : IJobProcessComponentData<Position, Dat_CustomRenderData> {
			public float s, c;

			public void Execute(ref Position position, [ReadOnly] ref Dat_CustomRenderData renderer) {
				float3 pos = position.Value;
				pos.x += c;
				pos.y += s;
				position.Value = pos;
			}
		}

		protected override JobHandle OnUpdate(JobHandle inputDeps) {
			var phi = Time.time * 2 * Mathf.PI * 2f;
			MoveJob job = new MoveJob {
				s = Mathf.Sin(phi)
				, c = Mathf.Cos(phi)
			};
			return job.Schedule(this, inputDeps);
		}
	}

}