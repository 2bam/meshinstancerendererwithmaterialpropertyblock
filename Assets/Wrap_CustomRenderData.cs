﻿using UnityEngine;
using Unity.Entities;
using UnityEngine.Rendering;
using System;

namespace com.iam2bam {

	[Serializable]
	struct Dat_CustomRenderData : IComponentData {
		public Vector4 color;
	}
	class Wrap_CustomRenderData : ComponentDataWrapper<Dat_CustomRenderData> { }

}