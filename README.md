# MeshInstanceRendererWithMaterialPropertyBlock #

Improved Unity's ECS MeshInstanceRendererSystem to handle MaterialPropertyBlocks for GPU instancing without changing shader semantics from "classic" instancing.

## Usage ##
You can test it as-is with the sample project.
* Shows colored moving instanced cubes
* Shows colored inert instanced cubes, when static
* Shows cyan cubes for non-instanced

Otherwise just drop in the files in *Assets* in your project, except Sys_Move.cs.
Make sure Package manager downloads Packages/manifest.json dependencies.

The code just works for color instancing. To customize for your own needs, change:

* `struct Dat_CustomRenderData` which stores per-entity values.
* `class Sys_MeshInstanceRendererWithMPB`
	* `class CustomDataCache` is a *structure of arrays* to cache per-entity values and add them to a MaterialPropertyBlock.
	* `CopyCustomDataToCache()` copies from entities' `Dat_CustomRenderData` component to `CustomDataCache`'s arrays.
	* `MaterialPropertyBlockFromCache()` sets MPB's internal arrays from `CustomDataCache`'s arrays with
		* SetFloatArray (float[])
		* SetVectorArray (Vector4[])
		* SetMatrixArray (Matrix4x4[])
* Add shader properties to `InstancedColorSurfaceShader` or your own with `UNITY_DEFINE_INSTANCED_PROP` & `UNITY_ACCESS_INSTANCED_PROP` macros.

## Notes ##

`Sys_MeshInstanceRendererBootstrap` disables the official MeshInstanceRendererSystem.
`Sys_MeshInstanceRendererWithoutMPB` is essentially the official renderer but for entities *WITHOUT* `Dat_CustomRenderData` to avoid system clashing.
`Sys_Move` and `TestManager` is just for the example, you can remove them.

**Read "MeshInstanceRendererSystem Dissected.txt" for information on how the official MeshInstanceRendererSystem works.**

## Versions ##

Tested on:

	Unity 2018.2.18f1
	com.unity.jobs@0.0.7-preview.5
	com.unity.entities@0.0.12-preview.21
	com.unity.collections@0.0.9-preview.10